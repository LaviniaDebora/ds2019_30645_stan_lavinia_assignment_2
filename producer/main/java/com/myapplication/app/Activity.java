package com.myapplication.app;

import org.json.simple.JSONObject;

public class Activity  {

    private Integer id;
    private String activity;
    private String start;
    private String end;

    Activity(Integer id, String activity, String start, String end){
        this.id=id;
        this.activity=activity;
        this.start=start;
        this.end=end;
    }

    public JSONObject  activityToJsonObject(){
        JSONObject json = new JSONObject();
        json.put("id",this.id);
        json.put("activity", this.activity);
        json.put("start",this.start);
        json.put("end",this.end);
        return json;
    }

}
