package com.myapplication.app;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class QueueConnection  {

    private String queue_name;
    private String host;

    QueueConnection(String queue_name, String host){
        this.queue_name=queue_name;
        this.host=host;
    }

    void sendMessage(JSONObject message) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(this.host);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(this.queue_name, false, false, false, null);

        channel.basicPublish("", this.queue_name, null, message.toJSONString().getBytes());
        System.out.println(" Sent : '" + message + "'");

        channel.close();
        connection.close();
    }
}
