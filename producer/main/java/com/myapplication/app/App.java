package com.myapplication.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import org.json.simple.JSONObject;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App
{
    private static final java.util.UUID UUID = null ;

    public static void main(String[] args ) throws IOException, TimeoutException {
        QueueConnection connection =new QueueConnection("daily_activities","localhost");

        Pattern twopart = Pattern.compile("(\\d+-\\d+-\\d+ \\d+:\\d+:\\d+)		(\\d+-\\d+-\\d+ \\d+:\\d+:\\d+)		([^+]+)(\t+)");
        String fileName="src/activity.txt";

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            Integer index=1;
            List<String> result = stream.collect(Collectors.toList());

            for(String str:result) {
                Matcher m = twopart.matcher(str);

                if (m.matches()) {

                    String d1 = m.group(1);
                    String d2 = m.group(2);
                    String act = m.group(3);

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    try {
                        Date date11 = formatter.parse(d1);
                        Date date22 = formatter.parse(d2);

                        String s1 = date11.toString();
                        String s2 = date22.toString();

                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
                        Date date1=simpleDateFormat.parse(s1); // Returns Date Format,
                        Date date2=simpleDateFormat.parse(s2);
                        SimpleDateFormat simpleDateFormat1=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"); // New Pattern

                        String uniqueID = UUID.randomUUID().toString();
                        Activity activity = new Activity(index,act,simpleDateFormat1.format(date1),simpleDateFormat1.format(date2));
                        index=index+1;
                        JSONObject json  = activity.activityToJsonObject();
                        connection.sendMessage(json);
                        Thread.sleep(1000);

                    } catch (ParseException | InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {
                    System.out.println(str + " does not match.");
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
