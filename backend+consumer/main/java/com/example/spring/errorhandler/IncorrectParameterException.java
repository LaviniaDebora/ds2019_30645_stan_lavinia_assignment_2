// imported from spring-demo project from /bitbucket.org/utcn_dsrl/spring-boot-demo/
package com.example.spring.errorhandler;

import java.util.List;

public class IncorrectParameterException extends RuntimeException {

    private static final String MESSAGE = "Incorrect Parameters";
    private final String resource;
    private final List<String> invalidParams;

    public IncorrectParameterException(String resource, List<String> errors) {
        super(MESSAGE);
        this.resource = resource;
        this.invalidParams = errors;
    }

    public List<String> getInvalidParams() {
        return invalidParams;

    }

    public String getResource() {
        return resource;
    }
}