package com.example.spring.entities;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "side_effects_list")
    @ElementCollection(targetClass=String.class)
    private List<String> sideEffects;

    @Column(name = "dosage")
    private Integer dosage;

    @ManyToOne
    private MedicationPlan medicationPlan;

    public Medication(){
    }

    public Medication(Integer id, String name, List<String> sideEffects, Integer dosage){
        this.id=id;
        this.name=name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

}
