package com.example.spring.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "startDate", length = 100)
    @Temporal(TemporalType.DATE)
    private Date start;

    @Column(name = "endDate", length = 100)
    @Temporal(TemporalType.DATE)
    private Date end;

    @OneToMany(mappedBy = "medicationPlan", cascade = CascadeType.ALL)
    private List<Medication> medicationList;

    @OneToOne
    private Patient patient;

    MedicationPlan(){}

    public MedicationPlan(Integer id, Date start, Date end){
        this.id=id;
        this.start=start;
        this.end=end;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return start;
    }

    public void setStartDate(Date start) {
        this.start = start;
    }

    public Date getEndDate() {
        return start;
    }

    public void setEndDate(Date start) {
        this.start = start;
    }

}
