package com.example.spring.entities;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false, length = 100)
    private String username;

    @Column(name = "password", nullable = false, length = 200)
    private String password;

    @Column(name = "role", nullable = false, length = 200)
    private String role;

    @OneToOne(mappedBy = "doctorUser", cascade = CascadeType.ALL)
    private Doctor doctor;

    @OneToOne(mappedBy = "patientUser", cascade = CascadeType.ALL)
    private Patient patient;

    @OneToOne(mappedBy = "caregiverUser", cascade = CascadeType.ALL)
    private Caregiver caregiver;

    public User(){
    }

    public User(Integer id, String username, String password, String role){
        this.id=id;
        this.username=username;
        this.password=password;
        this.role=role;
    }

    public User(Integer id, String username, String role) {
        this.id=id;
        this.username=username;
        this.role=role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsernameName(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
