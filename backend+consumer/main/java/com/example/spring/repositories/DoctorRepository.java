package com.example.spring.repositories;

import com.example.spring.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    @Query(value = "SELECT u " + "FROM Doctor u " + "ORDER BY u.name")
    List<Doctor> getAllOrdered();
}
