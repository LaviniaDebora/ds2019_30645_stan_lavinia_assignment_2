package com.example.spring.controller;

import com.example.spring.dto.MedicationPlanDTO;
import com.example.spring.dto.MedicationPlanViewDTO;
import com.example.spring.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationplan")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService  medicationPlanService) {
        this. medicationPlanService =  medicationPlanService;
    }

    @PostMapping(value="/{username}/create")
    public Integer insertMedicationPlanDTO(@PathVariable String username,@RequestBody MedicationPlanDTO  medicationPlanDTO){
        return  medicationPlanService.insert( medicationPlanDTO);
    }

    @GetMapping(value="/{username}/all")
    public List<MedicationPlanViewDTO> findAll(@PathVariable String username){
        return  medicationPlanService.findAll();
    }

    @GetMapping(value = "/{id}")
    public MedicationPlanViewDTO findById(@PathVariable("id") Integer id){
        return medicationPlanService.findMedicationPlanById(id);
    }

}
