package com.example.spring.controller;

import com.example.spring.dto.MedicationDTO;
import com.example.spring.dto.MedicationViewDTO;
import com.example.spring.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService  medicationService) {
        this. medicationService =  medicationService;
    }

    @PostMapping(value="/{username}/create")
    public Integer insertMedicationDTO(@PathVariable String username,@RequestBody MedicationDTO  medicationDTO){
        return  medicationService.insert( medicationDTO);
    }

    @GetMapping(value="/{username}/all")
    public List<MedicationViewDTO> findAll(@PathVariable String username){
        return  medicationService.findAll();
    }

    @GetMapping(value = "/{username}/{id}")
    public MedicationViewDTO findById(@PathVariable String username,@PathVariable("id") Integer id){
        return medicationService.findMedicationById(id);
    }

    @PutMapping(value="/{username}/update")
    public Integer updateUser(@PathVariable String username,@RequestBody MedicationDTO  medicationPlanDTO) {
        return  medicationService.update( medicationPlanDTO);
    }

    @DeleteMapping(value = "/{username}/delete/{id}")
    public void delete(@PathVariable String username,@PathVariable("id") Integer id){
        medicationService.delete(id);
    }
}
