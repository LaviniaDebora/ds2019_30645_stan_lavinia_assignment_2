package com.example.spring.controller;

import com.example.spring.dto.PatientDTO;
import com.example.spring.dto.PatientViewDTO;
import com.example.spring.services.PatientService;
import com.example.spring.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/patient")
public class PatientController {
    private final PatientService patientService;
    private final UserService userService;

    @Autowired
    public PatientController(PatientService patientService, UserService userService) {
        this.patientService = patientService;
        this.userService=userService;
    }

    @PostMapping(value="/{username}/create")
    public Integer insertPatientDTO(@PathVariable String username,@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @GetMapping(value="/{username}/all")
    public List<PatientViewDTO> findAll(@PathVariable String username){
        return patientService.findAll();
    }

    @GetMapping(value = "/{username}/{id}")
    public PatientViewDTO findById(@PathVariable String username,@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @PutMapping(value="/{username}/update")
    public Integer updatePatient(@PathVariable String username, @RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value = "/{username}/delete/{id}")
    public void delete(@PathVariable String username, @PathVariable("id") Integer id){
        patientService.delete(id);

    }
}
