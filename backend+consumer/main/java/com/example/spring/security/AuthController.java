package com.example.spring.security;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class AuthController {

    @GetMapping(path = "/basicauth")
    public AuthBean helloWorldBean() {
        return new AuthBean("User authenticated");
    }
}
