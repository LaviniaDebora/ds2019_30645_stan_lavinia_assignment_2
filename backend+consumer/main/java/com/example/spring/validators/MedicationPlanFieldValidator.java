package com.example.spring.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.example.spring.dto.MedicationPlanDTO;
import com.example.spring.errorhandler.IncorrectParameterException;
import java.util.List;
import java.util.ArrayList;

public class MedicationPlanFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(MedicationPlanFieldValidator.class);

    public static void validateInsertOrUpdate(MedicationPlanDTO medicationPlanDTO) {
        List<String> errors = new ArrayList<>();
        if (medicationPlanDTO == null) {
            errors.add("medicationPlanDTO is null");
            throw new IncorrectParameterException(MedicationPlanDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationPlanFieldValidator.class.getSimpleName(), errors);
        }
    }
}
