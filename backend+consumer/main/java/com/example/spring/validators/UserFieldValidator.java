package com.example.spring.validators;

import com.example.spring.dto.UserDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.example.spring.errorhandler.IncorrectParameterException;
import java.util.List;
import java.util.ArrayList;

public class UserFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(UserFieldValidator.class);

    public static void validateInsertOrUpdate(UserDTO userDTO) {
        List<String> errors = new ArrayList<>();
        if (userDTO == null) {
            errors.add("userDTO is null");
            throw new IncorrectParameterException(UserDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(UserFieldValidator.class.getSimpleName(), errors);
        }
    }
}
