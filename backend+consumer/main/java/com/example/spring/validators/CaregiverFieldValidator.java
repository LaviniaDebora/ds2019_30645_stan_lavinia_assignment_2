package com.example.spring.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.example.spring.dto.CaregiverDTO;
import com.example.spring.errorhandler.IncorrectParameterException;
import java.util.List;
import java.util.ArrayList;

public class CaregiverFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);

    public static void validateInsertOrUpdate(CaregiverDTO caregiverDTO) {
        List<String> errors = new ArrayList<>();
        if (caregiverDTO == null) {
            errors.add("caregiverDTO is null");
            throw new IncorrectParameterException(CaregiverDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(CaregiverFieldValidator.class.getSimpleName(), errors);
        }
    }
}
