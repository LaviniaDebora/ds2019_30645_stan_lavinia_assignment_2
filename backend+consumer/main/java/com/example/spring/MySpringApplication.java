package com.example.spring;

import com.example.spring.consumer.ActivityService;
import com.example.spring.consumer.QueueConnection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class MySpringApplication {

	public static void main(String[] args) throws IOException, TimeoutException {

		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(MySpringApplication.class, args);
		QueueConnection connection = new QueueConnection("daily_activities","localhost");
		connection.receiveMessage();

	}

}
