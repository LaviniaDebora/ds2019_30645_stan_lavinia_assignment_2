package com.example.spring.dto;

import com.example.spring.entities.Doctor;
import com.example.spring.entities.Patient;
import com.example.spring.entities.User;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class CaregiverDTO {

    private Integer id;
    private String name;
    private Date birthdate;
    private String gender;
    private String address;
    private Doctor caregiverDoctor;
    private List<Patient> patientsList;
    private User caregiverUser;

    public CaregiverDTO(){}

    public CaregiverDTO(Integer id, String name, Date birthdate, String gender, String address){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate(){return birthdate; }

    public void setBirthdate(Date birthdate){this.birthdate=birthdate; }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Doctor getCaregiverDoctor() { return caregiverDoctor; }

    public void setCaregiverDoctor(Doctor caregiverDoctor) { this.caregiverDoctor = caregiverDoctor; }

    public List<Patient> getPatientsList() { return patientsList; }

    public void setPatientsList(List<Patient> patientsList) { this.patientsList = patientsList; }

    public User getUser() { return caregiverUser; }

    public void setUser(User caregiverUser) { this.caregiverUser = caregiverUser; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(id, caregiverDTO.id) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(birthdate, caregiverDTO.birthdate) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(address, caregiverDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,birthdate, name, gender,address);
    }
}
