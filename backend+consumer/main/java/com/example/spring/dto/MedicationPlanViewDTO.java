package com.example.spring.dto;

import com.example.spring.entities.Medication;

import java.util.Date;
import java.util.List;

public class MedicationPlanViewDTO {
    private Integer id;
    private Date start;
    private Date end;

    public MedicationPlanViewDTO(Integer id, Date start, Date end ){
        this.id=id;
        this.start=start;
        this.end=end;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return start;
    }

    public void setStartDate(Date start) {
        this.start = start;
    }

    public Date getEndDate() {
        return start;
    }

    public void setEndDate(Date start) {
        this.start = start;
    }

}
