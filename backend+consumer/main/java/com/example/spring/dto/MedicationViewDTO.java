package com.example.spring.dto;

import java.util.List;

public class MedicationViewDTO {

    private Integer id;
    private String name;
    private List<String> sideEffects;
    private Integer dosage;

    public MedicationViewDTO(Integer id, String name, List<String> sideEffects, Integer dosage) {
        this.id=id;
        this.name=name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<String> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
