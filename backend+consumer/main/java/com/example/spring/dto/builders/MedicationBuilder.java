package com.example.spring.dto.builders;

import com.example.spring.dto.MedicationDTO;
import com.example.spring.entities.Medication;

public class MedicationBuilder {

    public MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medication medication){
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage());
    }
}
