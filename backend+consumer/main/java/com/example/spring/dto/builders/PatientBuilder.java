package com.example.spring.dto.builders;

import com.example.spring.dto.PatientDTO;
import com.example.spring.entities.Patient;

public class PatientBuilder {
    public PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalRecord());
    }
}
