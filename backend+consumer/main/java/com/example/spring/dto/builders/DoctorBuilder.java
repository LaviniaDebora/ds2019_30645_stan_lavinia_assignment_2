package com.example.spring.dto.builders;

import com.example.spring.dto.DoctorDTO;
import com.example.spring.entities.Doctor;

public class DoctorBuilder {

    public DoctorBuilder() {
    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName());
//                doctor.getCaregiversList());
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getId(),
                doctorDTO.getName());
 //               doctorDTO.getCaregiversList());
    }
}
