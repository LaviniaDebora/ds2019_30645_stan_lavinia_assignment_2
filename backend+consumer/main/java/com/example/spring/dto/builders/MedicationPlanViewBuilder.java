package com.example.spring.dto.builders;

import com.example.spring.dto.MedicationPlanViewDTO;
import com.example.spring.entities.MedicationPlan;

public class MedicationPlanViewBuilder {

    public static MedicationPlanViewDTO generateDTOFromEntity(MedicationPlan medication){
        return new MedicationPlanViewDTO(
                medication.getId(),
                medication.getStartDate(),
                medication.getEndDate());
//                medication.getMedicationList());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanViewDTO medicationViewDTO){
        return new MedicationPlan(
                medicationViewDTO.getId(),
                medicationViewDTO.getStartDate(),
                medicationViewDTO.getEndDate());
   //             medicationViewDTO.getMedicationList());
    }
}
