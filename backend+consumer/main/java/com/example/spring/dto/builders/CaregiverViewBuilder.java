package com.example.spring.dto.builders;

import com.example.spring.dto.CaregiverViewDTO;
import com.example.spring.entities.Caregiver;

public class CaregiverViewBuilder {

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getAddress());
    }

    public static Caregiver generateEntityFromDTO( CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(
                caregiverViewDTO.getId(),
                caregiverViewDTO.getName(),
                caregiverViewDTO.getBirthdate(),
                caregiverViewDTO.getGender(),
                caregiverViewDTO.getAddress());
    }
}
