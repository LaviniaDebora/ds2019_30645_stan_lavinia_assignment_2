package com.example.spring.dto.builders;

import com.example.spring.dto.MedicationDTO;
import com.example.spring.dto.MedicationViewDTO;
import com.example.spring.entities.Medication;

public class MedicationViewBuilder {

    public static MedicationViewDTO generateDTOFromEntity(Medication medication){
        return new MedicationViewDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationViewDTO medicationViewDTO){
        return new Medication(
                medicationViewDTO.getId(),
                medicationViewDTO.getName(),
                medicationViewDTO.getSideEffects(),
                medicationViewDTO.getDosage());
    }
}
