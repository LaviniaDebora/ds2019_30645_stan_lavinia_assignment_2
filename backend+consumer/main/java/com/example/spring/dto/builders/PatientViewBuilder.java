package com.example.spring.dto.builders;

import com.example.spring.dto.PatientViewDTO;
import com.example.spring.entities.Patient;

public class PatientViewBuilder {

    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord());
    }

    public static Patient generateEntityFromDTO( PatientViewDTO patientViewDTO){
        return new Patient(
                patientViewDTO.getId(),
                patientViewDTO.getName(),
                patientViewDTO.getBirthdate(),
                patientViewDTO.getGender(),
                patientViewDTO.getAddress(),
                patientViewDTO.getMedicalRecord());
    }
}
