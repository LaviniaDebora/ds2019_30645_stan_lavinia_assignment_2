package com.example.spring.dto.builders;

import com.example.spring.dto.DoctorViewDTO;
import com.example.spring.entities.Doctor;

public class DoctorViewBuilder {

    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(
                doctor.getId(),
                doctor.getName());
 //               doctor.getCaregiversList());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(
                doctorViewDTO.getId(),
                doctorViewDTO.getName());
//                doctorViewDTO.getCaregiversList());
    }
}
