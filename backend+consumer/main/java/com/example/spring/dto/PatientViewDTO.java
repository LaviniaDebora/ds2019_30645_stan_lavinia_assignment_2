package com.example.spring.dto;

import com.example.spring.entities.Caregiver;
import com.example.spring.entities.MedicationPlan;
import com.example.spring.entities.User;
import java.util.Date;
import java.util.List;

public class PatientViewDTO {

    private Integer id;
    private String name;
    private Date birthdate;
    private String gender;
    private String address;
    private List<String> medicalRecord;
    private Caregiver patientCaregiver;
    private User patientUser;
    private MedicationPlan medicationPlan;

    public PatientViewDTO(){}

    public PatientViewDTO(Integer id, String name, Date birthdate, String gender, String address,List<String> medicalRecord){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medicalRecord=medicalRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate(){return birthdate; }

    public void setBirthdate(Date birthdate){this.birthdate=birthdate; }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getMedicalRecord() { return medicalRecord; }

    public void setMedicalRecord(List<String> medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getPatientCaregiver() { return patientCaregiver; }

    public void setPatientCaregiver(Caregiver patientCaregiver) { this.patientCaregiver = patientCaregiver; }

    public User getUser() { return patientUser; }

    public void setUser(User patientUser) { this.patientUser = patientUser; }

    public MedicationPlan getMedicationPlan() { return medicationPlan; }

    public void setMedicationPlan(MedicationPlan medicationPlan) { this.medicationPlan = medicationPlan; }
}
