package com.example.spring.dto;

import com.example.spring.entities.Medication;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class MedicationPlanDTO {
    private Integer id;
    private Date start;
    private Date end;
   // private List<Medication> medicationList;

    public MedicationPlanDTO(Integer id, Date start, Date end ){
        this.id=id;
        this.start=start;
        this.end=end;
//        this.medicationList=medicationList;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return start;
    }

    public void setStartDate(Date start) {
        this.start = start;
    }

    public Date getEndDate() {
        return start;
    }

    public void setEndDate(Date start) {
        this.start = start;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO medicationPlanDTO = (MedicationPlanDTO) o;
        return Objects.equals(id, medicationPlanDTO.id) &&
                Objects.equals(start, medicationPlanDTO.start) &&
                Objects.equals(end, medicationPlanDTO.end);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, start, end);
    }
}
