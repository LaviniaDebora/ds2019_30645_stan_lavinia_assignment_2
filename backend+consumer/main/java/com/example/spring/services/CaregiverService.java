package com.example.spring.services;

import com.example.spring.dto.builders.CaregiverBuilder;
import com.example.spring.dto.builders.CaregiverViewBuilder;
import com.example.spring.entities.Caregiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.errorhandler.ResourceNotFoundException;
import com.example.spring.repositories.CaregiverRepository;
import com.example.spring.validators.CaregiverFieldValidator;
import com.example.spring.dto.CaregiverDTO;
import com.example.spring.dto.CaregiverViewDTO;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public List<CaregiverViewDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();
        return caregivers.stream().map(CaregiverViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public CaregiverViewDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", id);
        }
        return CaregiverViewBuilder.generateDTOFromEntity(caregiver.get());
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> caregiver= caregiverRepository.findById(caregiverDTO.getId());

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverDTO.getId().toString());
        }
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void delete(Integer id){
        this.caregiverRepository.deleteById(id);
    }
}
