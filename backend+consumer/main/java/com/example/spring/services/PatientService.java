package com.example.spring.services;

import com.example.spring.dto.builders.PatientBuilder;
import com.example.spring.dto.builders.PatientViewBuilder;
import com.example.spring.entities.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.errorhandler.ResourceNotFoundException;
import com.example.spring.repositories.PatientRepository;
import com.example.spring.validators.PatientFieldValidator;
import com.example.spring.dto.PatientDTO;
import com.example.spring.dto.PatientViewDTO;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;


@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Integer insert(PatientDTO patientDTO) {
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public List<PatientViewDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();
        return patients.stream().map(PatientViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public PatientViewDTO findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);
        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientViewBuilder.generateDTOFromEntity(patient.get());
    }

    public Integer update(Integer id,PatientDTO patientDTO) {

        Optional<Patient> patient= patientRepository.findById(id);

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient id", id.toString());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);

        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }
    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> patient= patientRepository.findById(patientDTO.getId());

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient id", patientDTO.getId().toString());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);

        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public void delete(Integer id){
        this.patientRepository.deleteById(id);
    }
}
