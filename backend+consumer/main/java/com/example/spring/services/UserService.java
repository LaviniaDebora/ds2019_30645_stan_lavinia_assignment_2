package com.example.spring.services;

import com.example.spring.dto.UserDTO;
import com.example.spring.dto.UserViewDTO;
import com.example.spring.dto.builders.UserBuilder;
import com.example.spring.dto.builders.UserViewBuilder;
import com.example.spring.entities.User;
import com.example.spring.repositories.UserRepository;
import com.example.spring.validators.UserFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.errorhandler.ResourceNotFoundException;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public List<UserViewDTO> findAll(){
        List<User> persons = userRepository.getAllOrdered();
        return persons.stream().map(UserViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public UserViewDTO findUserById(Integer id){
        Optional<User> user  = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User", "user id", id);
        }
        return UserViewBuilder.generateDTOFromEntity(user.get());
    }

    public UserViewDTO findUserByUsername(String username){
        List <User> users  = userRepository.findAll();
        Optional <User> user=null;
        for (User it : users) {
            if(it.getUsername().equals(username)){
                 user = Optional.of(it);
            }
        }
        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User", "username", username);
        }
        return UserViewBuilder.generateDTOFromEntity(user.get());
    }

}
