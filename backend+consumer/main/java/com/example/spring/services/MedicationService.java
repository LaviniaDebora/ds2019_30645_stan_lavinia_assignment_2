package com.example.spring.services;

import com.example.spring.dto.*;
import com.example.spring.dto.builders.*;
import com.example.spring.entities.Medication;
import com.example.spring.repositories.MedicationRepository;
import com.example.spring.validators.MedicationFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring.errorhandler.ResourceNotFoundException;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public Integer insert(MedicationDTO medicationDTO) {
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public List<MedicationViewDTO> findAll(){
        List<Medication> medicationPlans = medicationRepository.getAllOrdered();
        return medicationPlans.stream().map(MedicationViewBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public MedicationViewDTO findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);
        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", id);
        }
        return MedicationViewBuilder.generateDTOFromEntity(medication.get());
    }

    public Integer update(MedicationDTO medicationDTO) {

        Optional<Medication> medicationPlan= medicationRepository.findById(medicationDTO.getId());

        if(!medicationPlan.isPresent()){
            throw new ResourceNotFoundException("Medication", "medication id", medicationDTO.getId().toString());
        }
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);

        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void delete(Integer id){
        this.medicationRepository.deleteById(id);
    }
}
