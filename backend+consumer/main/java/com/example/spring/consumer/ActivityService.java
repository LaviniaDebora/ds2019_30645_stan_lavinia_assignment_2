package com.example.spring.consumer;

import com.example.spring.dto.PatientDTO;
import com.example.spring.dto.PatientViewDTO;
import com.example.spring.dto.builders.PatientBuilder;
import com.example.spring.dto.builders.PatientViewBuilder;
import com.example.spring.entities.Patient;
import com.example.spring.validators.ActivityFieldValidator;
import com.example.spring.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityService {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Integer insert(ActivityDTO activityDTO) {
        ActivityFieldValidator.validateInsertOrUpdate(activityDTO);
        return activityRepository.save(ActivityBuilder.generateEntityFromDTO(activityDTO)).getId();
    }

}
