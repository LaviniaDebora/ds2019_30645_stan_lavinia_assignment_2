package com.example.spring.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.concurrent.TimeoutException;

public class QueueConnection  {

    private String queue_name;
    private String host;

    @Autowired
    public QueueConnection(String queue_name, String host){
        this.queue_name=queue_name;
        this.host=host;
    }

    public void receiveMessage() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(this.host);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(this.queue_name, false, false, false, null);

        final Activity[] activity = new Activity[2];
        Service service = new Service("MyService");
        final Integer[] id = {1};

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag,
                                       Envelope envelope, AMQP.BasicProperties properties,
                                       byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" Received : '" + message + "'");

                String result = message.substring(message.indexOf("id\":") + 4, message.indexOf("}"));
                Integer patient_id = Integer.valueOf(result);
                ObjectMapper mapper = new ObjectMapper();
                try {
                    activity[1] = mapper.readValue(message, Activity.class);

                    String s1 = activity[1].getStart_date();
                    String s2 = activity[1].getEnd_date();
                    String activity_name = activity[1].getActivity_name();

                    service.insertActivity(id[0], activity_name, s1, s2);

                    Thread.sleep(1000);
                    String verification= service.verification(activity_name,s1,s2);
                  if(verification!=null){
                       service.insertNotification(id[0],verification);
                   }
                    id[0]++;
                    Thread.sleep(1000);

                } catch (IOException | InterruptedException | SQLException | ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        channel.basicConsume(this.queue_name, true, consumer);
    }
}

