package com.example.spring.consumer;


import com.example.spring.entities.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "notification")
public class Notification {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "message", nullable = false, length = 100)
    @JsonProperty
    private String message;


    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    private Patient notificationpatient;


    public Notification(Integer id, String message){
        this.id=id;
        this.message=message;
    }

    public Notification(){

    }

    public Integer getID(){
        return this.id;
    }

    public void setID(Integer id){
        this.id=id;
    }

    public String getMessage(){
        return this.message;
    }

    public void setMessage(String message){
        this.message=message;
    }

}
