package com.example.spring.consumer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Service {

    private String myservice;

    public Service(String myservice){
        this.myservice=myservice;
    }

    public void insertActivity(Integer id, String name, String date1format, String date2format ) throws SQLException {
        java.sql.Connection myConn=java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb","root","starwars");
        Statement myStm=myConn.createStatement();
        ResultSet myRes =myStm.executeQuery("select * from activity");
        String sql="insert into activity "+" (id,activity_name, start_date, end_date)"+"values ("+id+","+" '"+name+"',"+"'"+date1format+"', "+"'"+date2format+"')";
        myStm.executeUpdate(sql);
    }

    public void insertNotification(Integer id, String message) throws SQLException{

        java.sql.Connection myConn=java.sql.DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/mydb","root","starwars");
        Statement myStm=myConn.createStatement();
        ResultSet myRes =myStm.executeQuery("select * from notification");
        String sql="insert into notification "+" (id, message)"+"values ("+id+","+" '"+message+"')";
        myStm.executeUpdate(sql);

    }

    public String verification(String activity, String d1, String d2) throws ParseException {

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        Date date1=simpleDateFormat.parse(d1);
        Date date2=simpleDateFormat.parse(d2);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date1);
        Calendar calendar1 = new GregorianCalendar();
        calendar1.setTime(date2);

        int start = calendar.get(Calendar.HOUR);
        int end = calendar1.get(Calendar.HOUR);

        int startday = calendar.get(Calendar.DAY_OF_MONTH);
        int endday = calendar1.get(Calendar.DAY_OF_MONTH);
        int day = endday-startday;

        int startmin = calendar.get(Calendar.MINUTE);
        int endmin = calendar1.get(Calendar.MINUTE);

        int startm = 60 -start;
        int endm = 60-end;
        int minutes = startm + endm;

        int hours = Math.abs(end-start);

        String message=null;

        if(activity.equals("Toileting")){
            if(hours >= 1 || (minutes>60)){
                message = " # Notification:  "+activity+"  longer than 1 hours !";
                System.out.println(message);
            }
        }

        if(activity.equals("Showering")){
            if(hours>=1 || (minutes>60)){
                message = " # Notification:  "+activity+"  longer than 1 hours !";
                System.out.println(message);
            }
        }

        if(activity.equals("Leaving")){
            if(hours>=12 || day ==1){
                message = " # Notification:  "+activity+"  longer than 12 hours !";
                System.out.println(message);
            }
        }

        return message;

    }

}
