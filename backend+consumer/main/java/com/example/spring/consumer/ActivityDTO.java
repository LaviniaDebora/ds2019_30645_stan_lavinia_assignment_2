package com.example.spring.consumer;


import com.example.spring.entities.Patient;

import java.util.Date;
import java.util.Objects;

public class ActivityDTO {

    private Integer id;
    private String activity;
    private String start;
    private String end;
    private Patient activitypatient;

    public ActivityDTO(Integer id, String activity, String start, String end){
        this.id=id;
        this.activity=activity;
        this.start=start;
        this.end=end;
    }

    public ActivityDTO(){}


    public Integer getId(){
        return this.id;
    }

    public void setID(Integer id){
        this.id=id;
    }

    public String getActivity_name(){
        return this.activity;
    }

    public void setActivity_name(String activity_name) {
        this.activity = activity_name;
    }

    public String getEnd_date() {
        return this.end;
    }

    public void setEnd_date(String date){
        this.end=date;
    }

    public String getStart_date(){
        return this.start;
    }

    public void setStart_date(String date){
        this.start=date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityDTO activityDTO = (ActivityDTO) o;
        return Objects.equals(id, activityDTO.id) &&
                Objects.equals(activity, activityDTO.activity) &&
                Objects.equals(start, activityDTO.start) &&
                Objects.equals(end, activityDTO.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, activity, start, end);
    }
}
